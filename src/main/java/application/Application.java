package application;
import presentation.MainController;
import presentation.MainView;

public class Application {
	
	public static void main(String []args) {
		
		MainView mainView = new MainView();
		MainController controller = new MainController(mainView);
	}
}
