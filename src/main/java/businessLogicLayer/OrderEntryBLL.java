package businessLogicLayer;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import businessLogicLayer.validators.StockValidator;
import businessLogicLayer.validators.Validator;
import dataAccessLayer.OrderEntryDAO;
import model.Client;
import model.OrderEntry;

public class OrderEntryBLL {
private List<Validator<OrderEntry>> validators;
	
	public OrderEntryBLL() {
		validators = new ArrayList<Validator<OrderEntry>>();
		validators.add(new StockValidator());
	}
	
	/**
	 * inserts an order entry into the database
	 * @param orderEntry the order entry to be inserted
	 * @throws IllegalArgumentException if the quantity is greater than the stock
	 */
	public void insertOrderEntry(OrderEntry orderEntry) throws IllegalArgumentException{
		OrderEntryDAO dao = new OrderEntryDAO();
		for(Validator<OrderEntry> v : validators) {
			v.validate(orderEntry);
		}
		dao.insert(orderEntry);
	}
	
	/**
	 * finds all order entries having a specific order id
	 * @param id the specific order id
	 * @return
	 */
	public List<OrderEntry> findOrderEntriesByOrderId(int id){
		OrderEntryDAO dao = new OrderEntryDAO();
		System.out.println("id " + id);
		List<OrderEntry> entries = dao.findEntriesByOrderId(id);
		if(entries == null || entries.size() == 0) {
			System.out.println(entries);
			throw new NoSuchElementException("No entries for order with id = " + id + " have been found!");
		}
		return entries;
	}
	
	/**
	 * finds order entries having a specific product id
	 * @param id the specific product id
	 * @return a lists of entries
	 */
	public List<OrderEntry> findOrderEntriesByProductId(int id){
		OrderEntryDAO dao = new OrderEntryDAO();
		List<OrderEntry> entries = dao.findEntriesByProductId(id);
		if(entries == null || entries.size() == 0)
			throw new NoSuchElementException("No entries for product with id = " + id + " have been found!");
		return entries;
	}
	
}
