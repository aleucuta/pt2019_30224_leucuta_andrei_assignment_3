package businessLogicLayer;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import businessLogicLayer.validators.Validator;
import dataAccessLayer.ProductDAO;
import model.Product;

public class ProductBLL {
	
	private List<Validator<Product>> validators;
	
	public ProductBLL() {
		validators = new ArrayList<Validator<Product>>();
	}
	
	/**
	 * finds the product having a specific id
	 * @param id the specific id
	 * @return
	 */
	public Product findProductById(int id) {
		ProductDAO dao = new ProductDAO();
		Product product = dao.findById(id);
		if(product == null) {
			throw new NoSuchElementException("The product with id = " + id + " was not found!");
		}
		return product;
	}
	
	/**
	 * finds and returns a list of products
	 * @return a list of products
	 */
	public List<Product> listProducts(){
		ProductDAO dao = new ProductDAO();
		List<Product> products = dao.findAll();
		return products;
	}
	
	/**
	 * updates a product having a specific id
	 * @param product the new data of the product to be updated
	 * @param id the specific id
	 */
	public void updateProductOnId(Product product,Integer id) {
		ProductDAO dao = new ProductDAO();
		for(Validator<Product> v : validators) {
			v.validate(product);
		}
		dao.updateOnId(product,id);
	}
	
	/**
	 * inserts a product into the database
	 * @param product the product to be inserted
	 */
	public void insertProduct(Product product) {
		ProductDAO dao = new ProductDAO();
		for(Validator<Product> v : validators) {
			v.validate(product);
		}
		dao.insert(product);
	}

	/**
	 * deletes a product having a specific id
	 * @param id the specific id
	 */
	public void deleteById(int id) {
		ProductDAO dao = new ProductDAO();
		dao.deleteById(id);
	}
}
