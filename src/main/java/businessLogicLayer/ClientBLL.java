package businessLogicLayer;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import businessLogicLayer.validators.Validator;
import dataAccessLayer.ClientDAO;
import model.Client;

public class ClientBLL {
	
	private List<Validator<Client>> validators;
	
	public ClientBLL() {
		validators = new ArrayList<Validator<Client>>();	
	}
	
	/**
	 * finds a client having a specific id
	 * @param id the id of the client to be found
	 * @return a client with a specific id
	 */
	public Client findClientById(int id) {
		ClientDAO dao = new ClientDAO();
		Client client = dao.findById(id);
		if(client == null) {
			throw new NoSuchElementException("The client with id = " + id + " was not found!");
		}
		return client;
	}
	
	/**
	 * inserts a client into the database
	 * @param client the client to be inserted
	 */
	public void insertClient(Client client) {
		ClientDAO dao = new ClientDAO();
		for(Validator<Client> v : validators) {
			v.validate(client);
		}
		dao.insert(client);
	}
	
	/**
	 * updates the client into the database that has a specific id
	 * @param client the new client data
	 * @param id the id of the client to be updated
	 */
	public void updateClientOnId(Client client,int id) {
		ClientDAO dao = new ClientDAO();
		for(Validator<Client> v : validators) {
			v.validate(client);
		}
		dao.updateOnId(client, id);
	}
	
	/**
	 * deletes a client having a specific id
	 * @param id the id of the client to be deleted
	 */
	public void deleteById(Integer id) {
		ClientDAO dao = new ClientDAO();
		dao.deleteById(id);
	}
	
	/**
	 * finds and returns the all the clients
	 * @return a list of clients
	 */
	public List<Client> listClients(){
		ClientDAO dao = new ClientDAO();
		List<Client> clients = dao.findAll();
		return clients;
	}

}
