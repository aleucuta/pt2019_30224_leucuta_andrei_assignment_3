package businessLogicLayer.validators;

public interface Validator <T> {
	
	/**
	 * an abstract method that should validate a data set
	 * @param t the data set to be validated
	 */
	public void validate(T t);
}
