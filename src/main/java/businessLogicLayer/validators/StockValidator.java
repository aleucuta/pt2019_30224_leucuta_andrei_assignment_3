package businessLogicLayer.validators;

import businessLogicLayer.ProductBLL;
import model.OrderEntry;
import model.Product;

public class StockValidator implements Validator<OrderEntry>{

	@Override
	public void validate(OrderEntry t) {
		// TODO Auto-generated method stub
		ProductBLL productBLL = new ProductBLL();
		Product product = productBLL.findProductById(t.getProduct_id());
		if(product.getStock() < t.getQuantity()) {
			System.out.println("Under stock!");
			throw new IllegalArgumentException("Under stock!");
			
		}
	}
	
}
