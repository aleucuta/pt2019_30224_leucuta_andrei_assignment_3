package businessLogicLayer;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import businessLogicLayer.validators.Validator;
import dataAccessLayer.ProductsOrderDAO;
import model.Client;
import model.ProductsOrder;

public class ProductsOrderBLL {
	
	private List<Validator<ProductsOrder>> validators;
	
	public ProductsOrderBLL() {
		validators = new ArrayList<Validator<ProductsOrder>>();	
	}
	
	/**
	 * finds and returns an order with a specific id
	 * @param id the id of the order to be found
	 * @return the order having the id = id
	 */
	public ProductsOrder findProductsOrderById(int id) {
		ProductsOrderDAO dao = new ProductsOrderDAO();
		ProductsOrder order = dao.findById(id);
		if(order == null) {
			throw new NoSuchElementException("The order with id = " + id + " was not found!");
		}
		return order;
	}
	/**
	 * updates an order with a specific id
	 * @param order the new data for the order
	 * @param id the id of the order to be updated
	 */
	public void updateProductsOrderOnId(ProductsOrder order,int id) {
		ProductsOrderDAO dao = new ProductsOrderDAO();
		for(Validator<ProductsOrder> each : validators) {
			each.validate(order);
		}
		dao.updateOnId(order, id);
	}
	
	/**
	 * inserts an order
	 * @param order the order to be inserted
	 */
	public void insertProductsOrder(ProductsOrder order) {
		ProductsOrderDAO dao = new ProductsOrderDAO();
		for(Validator<ProductsOrder> each : validators) {
			each.validate(order);
		}
		dao.insert(order);
	}
	
	/**
	 * Finds orders for a client id
	 * @param id the client id to search orders on
	 * @return a list of orders
	 */
	public List<ProductsOrder> findProductsOrderForClient(int id){
		ProductsOrderDAO dao = new ProductsOrderDAO();
		List<ProductsOrder> orders = dao.findOrdersByClient(id);
		if(orders == null || orders.size() == 0) {
			throw new NoSuchElementException("No orders for client with id = " + id + " were found!");
		}
		return orders;
	}
	/**
	 * Finds and returns all orders
	 * @return a list of orders
	 */
	public List<ProductsOrder> listProductsOrders(){
		ProductsOrderDAO dao = new ProductsOrderDAO();
		List<ProductsOrder> orders = dao.findAll();
		return orders;
	}
	/**
	 * Deletes on id
	 * @param id id of the object to be deleted
	 */
	public void deleteById(int id) {
		ProductsOrderDAO dao = new ProductsOrderDAO();
		dao.deleteById(id);
	}
}
