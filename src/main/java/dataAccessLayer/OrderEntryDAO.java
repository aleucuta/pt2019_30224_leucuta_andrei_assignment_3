package dataAccessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import model.OrderEntry;

public class OrderEntryDAO extends AbstractDAO<OrderEntry>{
	
	/**
	 * overrode the methods so I don't use them, leading to crashes
	 */
	@Override
	public void deleteById(int id) { // DONT USE THIS
		return; 
	}
	
	/**
	 * overrode the methods so I don't use them, leading to crashes
	 */
	@Override
	public OrderEntry findById(int id) { //DONT USE THIS
		return null;
	}
	
	/**
	 * overrode the methods so I don't use them, leading to crashes
	 */
	@Override
	public void updateOnId(OrderEntry object,int id) { //DONT USE THIS
		return ;
	}
	
	/**
	 * finds order entries by a specific id
	 * @param id the specific id
	 * @return a list of order entries
	 */
	public List<OrderEntry> findEntriesByOrderId(int id){
		List<OrderEntry> entries = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("Order_id");
		try {
			connection = ConnectionBuilder.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			System.out.println(statement);
			resultSet = statement.executeQuery();
			entries = createObjects(resultSet);
			return entries;
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionBuilder.close(statement);
			ConnectionBuilder.close(resultSet);
			ConnectionBuilder.close(connection);
		}
		return null;
	}
	
	/**
	 * finds order entries by a specific product id
	 * @param id the specific product id
	 * @return a list of order entries
	 */
	public List<OrderEntry> findEntriesByProductId(int id){
		List<OrderEntry> entries = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("Product_id");
		try {
			connection = ConnectionBuilder.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			entries = createObjects(resultSet);
			return entries;
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionBuilder.close(statement);
			ConnectionBuilder.close(resultSet);
			ConnectionBuilder.close(connection);
		}
		return null;
	}
}
