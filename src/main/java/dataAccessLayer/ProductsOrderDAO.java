package dataAccessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import model.ProductsOrder;

public class ProductsOrderDAO extends AbstractDAO<ProductsOrder>{
	
	/**
	 * finds an order by a specific client id
	 * @param id the specific client id
	 * @return a list of products orders
	 */
	public List<ProductsOrder> findOrdersByClient(int id){
		List<ProductsOrder> productsOrder = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("Client_id");
		try {
			connection = ConnectionBuilder.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			productsOrder = createObjects(resultSet);
			return productsOrder;
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionBuilder.close(statement);
			ConnectionBuilder.close(resultSet);
			ConnectionBuilder.close(connection);
		}
		return null;
	}
	
}
