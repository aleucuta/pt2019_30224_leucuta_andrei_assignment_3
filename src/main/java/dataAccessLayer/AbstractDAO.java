package dataAccessLayer;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class AbstractDAO<T> {

	private final Class<T> type;

	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		
	}
	/**
	 * creates a select query
	 * @param field - the field to select on
	 * @return the query
	 */
	protected String createSelectQuery(String field) {
		StringBuilder sb = new StringBuilder();
		return sb.append("SELECT ").append("* ").append("FROM ").append(type.getSimpleName())
				.append(" WHERE " + field + " =?").toString();
	}

	/**
	 * creates a select *(all) query 
	 * @return the query
	 */
	private String createSelectAllQuery() {
		StringBuilder sb = new StringBuilder();
		return sb.append("SELECT ").append("* ").append("FROM ").append(type.getSimpleName()).toString();
	}
	
	/**
	 * creates a delete query
	 * @param field the field to delete on
	 * @return the query
	 */
	private String createDeleteQuery(String field) {
		StringBuilder sb = new StringBuilder();
		return sb.append("DELETE ").append("FROM ").append(type.getSimpleName()).append(" WHERE " + field + " =?")
				.toString();
	}

	/**
	 * creates an insert query
	 * @return the query
	 */
	private String createInsertQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT ").append("INTO ").append(type.getSimpleName()).append(" ( ");
		for (Field field : type.getDeclaredFields()) {
			sb.append(field.getName()).append(",");
		}
		
		sb.deleteCharAt(sb.length() - 1);
		sb.append(") ").append(" VALUES ").append(" ( ");
		for (@SuppressWarnings("unused") Field field : type.getDeclaredFields()) {
			sb.append("?").append(",");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append(") ");
		return sb.toString();
	}
	
	/**
	 * creates an update query
	 * @param field the field to update on
	 * @return the query
	 */
	private String createUpdateQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE ").append(type.getSimpleName()).append(" SET ");
		for (Field each_field : type.getDeclaredFields()) {
			if(each_field.getName() != "id")
				sb.append(each_field.getName()).append(" = ").append(" ?,");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append(" WHERE ").append(field.toString()).append(" =?");
		return sb.toString();
	}
	
	/**
	 * creates a list of objects from a result set of a query execution
	 * @param resultSet the result set
	 * @return a lists of object
	 */
	protected List<T> createObjects(ResultSet resultSet) {
		List<T> list = new ArrayList<T>();
		try {
			while (resultSet.next()) {
				T instance = type.newInstance();
				for (Field field : type.getDeclaredFields()) {
					try {
						if(resultSet.getString(field.getName()) != null) {
							Object value = resultSet.getObject(field.getName());
							if (value != null) {
								PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
								Method method = propertyDescriptor.getWriteMethod();
								//System.out.println(method);
								method.invoke(instance, value);
							}
						} 
					}catch(java.sql.SQLException e) {
					}
				}
				list.add(instance);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	/**
	 * inserts an object into the database
	 * @param object the object to be inserted
	 */
	public void insert(T object) {
		Connection connection = null;
		PreparedStatement statement = null;
		String query = createInsertQuery();
		try {
			connection = ConnectionBuilder.getConnection();
			statement = connection.prepareStatement(query);
			////System.out.println(query);
			int i = 1;
			for (Field field : object.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				statement.setObject(i, field.get(object));
				i = i + 1;
			}
			System.out.println(statement.toString());
			statement.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionBuilder.close(statement);
			ConnectionBuilder.close(connection);
		}
	}

	/**
	 * updates an object having a specific id
	 * @param Object the new data of the object to be updated
	 * @param id the specific id
	 */
	public void updateOnId(T Object, int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		String query = createUpdateQuery("id");
		try {
			connection = ConnectionBuilder.getConnection();
			statement = connection.prepareStatement(query);
			//System.out.println(query);
			int i = 1;
			for (Field field : Object.getClass().getDeclaredFields()) {
				if (field.getName().equals("id") == false) {
					field.setAccessible(true);
					statement.setObject(i, field.get(Object));
					i = i + 1;
				}
			}
			statement.setInt(i, id);
			System.out.println(statement.toString());
			statement.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionBuilder.close(statement);
			ConnectionBuilder.close(connection);
		}
	}

	/**
	 * finds an object by a specific id
	 * @param id the id
	 * @return the object
	 */
	public T findById(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("id");
		try {
			connection = ConnectionBuilder.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			//System.out.println(statement.toString());
			resultSet = statement.executeQuery();
			//if(resultSet.first() == false)
			//	return null;
			//System.out.println("YO" + (resultSet.isBeforeFirst() && resultSet.isAfterLast()));
			List<T> resultedObject = (List<T>) createObjects(resultSet);
			if (resultedObject.isEmpty() != true)
				return resultedObject.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionBuilder.close(statement);
			ConnectionBuilder.close(resultSet);
			ConnectionBuilder.close(connection);
		}
		return null;
	}

	/**
	 * finds all the objects 
	 * @return a lists of the objects
	 */
	public List<T> findAll() {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectAllQuery();
		try {
			connection = ConnectionBuilder.getConnection();
			statement = connection.prepareStatement(query);
			//System.out.println(statement.toString());
			resultSet = statement.executeQuery();
			List<T> resultedObject = (List<T>) createObjects(resultSet);
			if (resultedObject.isEmpty() != true)
				return resultedObject;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionBuilder.close(statement);
			ConnectionBuilder.close(resultSet);
			ConnectionBuilder.close(connection);
		}
		return null;
	}
	
	/**
	 * deletes an object having a specific id
	 * @param id the specific id
	 */
	public void deleteById(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		String query = createDeleteQuery("id");
		try {
			connection = ConnectionBuilder.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			System.out.println(statement.toString());
			statement.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionBuilder.close(statement);
			ConnectionBuilder.close(connection);
		}
	}
}
