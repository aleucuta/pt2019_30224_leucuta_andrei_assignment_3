package model;

public class OrderEntry {
	private Integer Order_id;
	private Integer Product_id;
	private Integer quantity;
	
	public OrderEntry() {
		
	}
	
	public OrderEntry(int Order_id ,int Product_id, int quantity) {
		this.Order_id = Order_id;
		this.Product_id = Product_id;
		this.quantity = quantity;
	}
	
	public int getOrder_id() {
		return Order_id;
	}

	public void setOrder_id(int order_id) {
		Order_id = order_id;
	}

	public int getProduct_id() {
		return Product_id;
	}

	public void setProduct_id(int product_id) {
		Product_id = product_id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
