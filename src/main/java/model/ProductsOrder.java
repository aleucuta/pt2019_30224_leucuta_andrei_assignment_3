package model;

public class ProductsOrder {
	private Integer id;
	private Integer Client_id;
	private String address;
	private Double orderValue;
	
	public ProductsOrder() {
		
	}
	
	public ProductsOrder(int Client_id,String address,double orderPrice) {
		//this.id = id;
		this.Client_id = Client_id;
		this.address = address;
		this.orderValue = orderPrice;
	}
	
	public ProductsOrder(int id,int Client_id,String address,double orderPrice) {
		this.id = id;
		this.Client_id = Client_id;
		this.address = address;
		this.orderValue = orderPrice;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getClient_id() {
		return Client_id;
	}

	public void setClient_id(int client_id) {
		Client_id = client_id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public double getOrderValue() {
		return orderValue;
	}

	public void setOrderValue(double orderValue) {
		this.orderValue = orderValue;
	}
	
}
