package model;

public class Address {
	private int id;
	private String country;
	private String region;
	private String city;
	private String street;
	
	public Address() {
		
	}
	
	public Address(String city,String country,String region,String street) {
		this.city = city;
		this.country = country;
		this.region = region;
		this.street = street;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	@Override
	public String toString() {
		return Integer.toString(this.id) + ":" + this.country + "," + this.region + "," + this.city + "," + this.street + ";";
	}
}
