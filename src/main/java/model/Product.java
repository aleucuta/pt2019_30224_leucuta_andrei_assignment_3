package model;

public class Product {
	private Integer id;
	private String name;
	private Double price;
	private String description;
	private Integer stock;
	
	
	public Product() {
		
	}
	
	public Product(String name,double price,String description,int stock) {
		this.name = name;
		this.price = price;
		this.description = description;
		this.stock = stock;
	}
	
	public Product(int id,String name,double price,String description,int stock) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.description = description;
		this.stock = stock;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	
	@Override
	public String toString() {
		return this.name + ":" + String.valueOf(this.price) + "; available: " + Integer.toString(this.stock)
 + ". Description of product: " + this.description;	}
	
}
