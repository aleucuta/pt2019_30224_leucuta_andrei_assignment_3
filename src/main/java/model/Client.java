package model;

public class Client {
	private Integer id;
	private String firstName;
	private String lastName;
	private Integer numberOfOrders;
	private Double totalAmountPaid;
	
	public Client() {
		
	}
	
//	public Client(int nrOrders,double totalPaid) {
//		//this.id = id;
//		this.numberOfOrders = nrOrders;
//		this.totalAmountPaid = totalPaid;
//	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	

	public int getNumberOfOrders() {
		return this.numberOfOrders;
	}

	public void setNumberOfOrders(int nrOrders) {
		this.numberOfOrders = nrOrders;
	}

	public double getTotalAmountPaid() {
		return totalAmountPaid;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setTotalAmountPaid(double totalPaid) {
		this.totalAmountPaid = totalPaid;
	}
	
	@Override
	public String toString() {
		return Integer.toString(this.id) + ":" + "nr orders-" + Integer.toString(this.numberOfOrders) + ", total paid-" + Double.toString(this.totalAmountPaid);
	}
}
