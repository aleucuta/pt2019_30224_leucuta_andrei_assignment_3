package presentation;

import javax.swing.JFrame;
import businessLogicLayer.ClientBLL;
import businessLogicLayer.ProductBLL;
import businessLogicLayer.ProductsOrderBLL;
import model.Client;
import model.Product;
import model.ProductsOrder;

import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;
import java.awt.Component;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JEditorPane;

public class MainView extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTable mainTable;
	private JTable secondaryTable;
	private JScrollPane mainTablePanel;
	private JScrollPane secondaryTablePanel ;
	private JPanel secondaryButtonsPanel;
	private JButton btnUpdate;
	private JButton btnDelete;
	private JButton btnAdd;
	private JButton btnListOrders;
	private JButton btnListClients;
	private JButton btnListProducts;
	private JButton btnAddNewOrder;
	private JButton btnPrintOrder;
	private JEditorPane instructionsTextField;
	/**
	 * Launch the application.
	 */
	/**
	 * Create the application.
	 */
	public MainView() {
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.setBounds(100, 100, 699, 412);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);

		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setBounds(10, 11, 119, 341);
		this.getContentPane().add(buttonsPanel);

		mainTablePanel = new JScrollPane();
		mainTablePanel.setBounds(139, 11, 534, 209);
		getContentPane().add(mainTablePanel);
		
		mainTable = new JTable();
		mainTable.setCellSelectionEnabled(true);
		buttonsPanel.setLayout(null);
			
		btnListOrders = new JButton("List Orders");
		btnListOrders.setBounds(1, 5, 117, 25);
		buttonsPanel.add(btnListOrders);
		
		btnListProducts= new JButton("List Products");
		btnListProducts.setBounds(1, 35, 117, 25);
		buttonsPanel.add(btnListProducts);
		
		btnListClients = new JButton("List Clients");
		btnListClients.setBounds(1, 65, 117, 25);
		buttonsPanel.add(btnListClients);
		
		btnAddNewOrder = new JButton("Add Order");
		btnAddNewOrder.setBounds(1, 95, 117, 25);
		buttonsPanel.add(btnAddNewOrder);
		
		btnPrintOrder = new JButton("Print Order");
		btnPrintOrder.setBounds(1, 125, 117, 25);
		buttonsPanel.add(btnPrintOrder);
		
		instructionsTextField = new JEditorPane();
		instructionsTextField.setEditable(false);
		instructionsTextField.setText("Instructions will be displayed here!");
		instructionsTextField.setBounds(0, 244, 118, 97);
		buttonsPanel.add(instructionsTextField);
		
		secondaryTablePanel = new JScrollPane();
		secondaryTablePanel.setBounds(139, 248, 534, 114);
		getContentPane().add(secondaryTablePanel);
		
		secondaryTable = new JTable();
		secondaryTablePanel.setViewportView(secondaryTable);
		
		secondaryButtonsPanel = new JPanel();
		secondaryButtonsPanel.setBounds(139, 220, 534, 27);
		getContentPane().add(secondaryButtonsPanel);
		secondaryButtonsPanel.setLayout(null);
		
		btnDelete = new JButton("DELETE");
		btnDelete.setBounds(23, 0, 98, 27);
		secondaryButtonsPanel.add(btnDelete);
		
		btnUpdate = new JButton("UPDATE");
		btnUpdate.setBounds(219, 0, 98, 27);
		secondaryButtonsPanel.add(btnUpdate);
		
		btnAdd = new JButton("ADD");
		btnAdd.setBounds(407, 0, 98, 27);
		secondaryButtonsPanel.add(btnAdd);
		
	}
	
	public void btnUpdateActionListener(final ActionListener actionListener){
		btnUpdate.addActionListener(actionListener);
	}
	public void btnDeleteActionListener(final ActionListener actionListener) {
		btnDelete.addActionListener(actionListener);
	}
	public void btnAddActionListener(final ActionListener actionListener){
		btnAdd.addActionListener(actionListener);
	}
	public void btnListOrdersActionListener(final ActionListener actionListener){
		btnListOrders.addActionListener(actionListener);
	}
	public void btnListClientsActionListener(final ActionListener actionListener){
		btnListClients.addActionListener(actionListener);
	}
	public void btnListProductsActionListener(final ActionListener actionListener){
		btnListProducts.addActionListener(actionListener);
	}
	public void btnAddNewOrderActionListener(final ActionListener actionListener) {
		btnAddNewOrder.addActionListener(actionListener);
	}
	public void btnPrintOrderActionListener(final ActionListener actionListener) {
		btnPrintOrder.addActionListener(actionListener);
	}
	
	public JTable getSecondaryTable() {
		return secondaryTable;
	}
	public void setSecondaryTable(JTable table) {
		secondaryTable = table;
	}
	
	public JTable getMainTable() {
		return mainTable;
	}
	
	public void setMainTable(JTable table) {
		mainTable = table;
	}
	
	public JScrollPane getMainTablePanel() {
		return mainTablePanel;
	}
	
	public JScrollPane getSecondaryTablePanel() {
		return secondaryTablePanel;
	}
	
	public void setInstructiosnText(String text) {
		instructionsTextField.setText(text);
	}
	public void displayErrorMessage(String errorText){
		final JPanel panel = new JPanel();

	    JOptionPane.showMessageDialog(panel, errorText, "Error", JOptionPane.ERROR_MESSAGE);
	}
}
