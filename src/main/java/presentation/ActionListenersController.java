package presentation;

import java.util.List;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import businessLogicLayer.ClientBLL;
import businessLogicLayer.ProductBLL;
import businessLogicLayer.ProductsOrderBLL;
import model.Client;
import model.Product;
import model.ProductsOrder;
import presentation.MainController.STANCE;

public class ActionListenersController {
	private MainView mainView;
	private TableController tableController;
	private MainController mainController;
	
	public ActionListenersController(MainController mainController,TableController tableController,MainView mainView) {
		this.mainView = mainView;
		this.tableController = tableController;
		this.mainController = mainController;
	}
	
	/**
	 * initializes all the action listeners
	 */
	void initializeActionListeners(){
		mainView.btnListClientsActionListener(btnListClientsActionListener());
		mainView.btnListOrdersActionListener(btnListOrdersActionListener());
		mainView.btnListProductsActionListener(btnListProductsActionListener());
		mainView.btnAddNewOrderActionListener(btnAddNewOrderActionListener());
		mainView.btnDeleteActionListener(btnDeleteActionListener());
		mainView.btnAddActionListener(btnAddActionListener());
		mainView.btnUpdateActionListener(btnUpdateActionListener());
		mainView.btnPrintOrderActionListener(btnPrintOrderActionListener());
	}
	
	/**
	 * creates and returns an action listener for the List Clients button that updates the main table, 
	 * sets the program stance and updates the instruction text box
	 * @return the action listener
	 */
	private ActionListener btnListClientsActionListener() {
		return(e->{
			tableController.updateMainTable(Client.class,ClientBLL.class);
			mainController.setTableStance(STANCE.CLIENTS);
			mainView.setInstructiosnText("Select a client and then right click him for details.");
		});
	}
	
	/**
	 * creates and returns an action listener for the List Products button that updates the main table, 
	 * sets the program stance and updates the instruction text box 
	 * @return the action listener
	 */
	private ActionListener btnListProductsActionListener() {
		return(e->{	
			tableController.updateMainTable(Product.class,ProductBLL.class);
			mainController.setTableStance(STANCE.PRODUCTS);
			mainView.setInstructiosnText("Select a product and then right it for details.");
		});
	}
	
	/**
	 * creates and returns an action listener for the List Orders button that updates the main table, 
	 * sets the program stance and updates the instruction text box
	 * @return the action listener
	 */
	private ActionListener btnListOrdersActionListener() {
		return(e->{
			tableController.updateMainTable(ProductsOrder.class,ProductsOrderBLL.class);
			mainController.setTableStance(STANCE.ORDERS);
			mainView.setInstructiosnText("Select an order and then right click it for details.");
		});
	}
	
	/**
	 * creates and returns an action listener for the Delete button that invokes the delete method
	 * from the MainController class on the id that it gets from the selected row in the main table
	 * @return the action listener
	 */
	private ActionListener btnDeleteActionListener() {
		return (e->{
			JTable mainTable = mainView.getMainTable();
			int id = Integer.valueOf((String)((JTable) mainTable).getValueAt(((JTable) mainTable).getSelectedRow(), 0));
			STANCE tableStance = mainController.getTableStance();
			mainController.deleteByStance(id);
			switch(tableStance) {
				case CLIENTS:
					tableController.updateMainTable(Client.class, ClientBLL.class);
					break;
				case PRODUCTS:
					tableController.updateMainTable(Product.class,ProductBLL.class);
					break;
				case ORDERS:
					tableController.updateMainTable(ProductsOrder.class,ProductsOrderBLL.class);
					break;
				default:
					break;
			}
			mainView.setInstructiosnText("Item deleted!");
		});
	}
	/**
	 * creates and returns an action listener for the Add New Order button that sets the program stance, updates
	 * the main table, and updates the instruction text box
	 * @return the action listener
	 */
	private ActionListener btnAddNewOrderActionListener() {
		return(e->{
			mainController.setTableStance(STANCE.NEWORDER_CLIENT);
			tableController.updateMainTable(Client.class,ClientBLL.class);
			mainView.setInstructiosnText("Select a client and then right click him.");
		});
	}
	
	/**
	 * creates and returns an action listener for the Add button that sets the program stance and adds another row
	 * to the main table to be filled with new data so as the new data can be inserted into the database
	 * @return the action listener
	 */
	private ActionListener btnAddActionListener() {
		return(e->{
			STANCE tableStance = mainController.getTableStance();
			if(tableStance == STANCE.CLIENTS || tableStance == STANCE.PRODUCTS) {
				JTable mainTable = mainView.getMainTable();
				DefaultTableModel model = (DefaultTableModel)(mainTable.getModel());
				int rowCount = mainTable.getRowCount();
				String[] spaces = new String[rowCount];
				for(int i=0;i<rowCount;i++)
					spaces[i] = "";
				model.addRow(spaces);
				mainView.setInstructiosnText("Fill the empty fields and then click Update.");
			}
		});
	}
	/**
	 * creates and returns an action listener for the Update button that invokes the updateDB method from the MainController
	 * class based on the program's stance
	 * @return an action listener
	 */
	private ActionListener btnUpdateActionListener() {
		return(e->{
			JTable mainTable = mainView.getMainTable();
			int columnCount = mainTable.getColumnCount();
			Object[] rowData = new Object[columnCount];
			STANCE tableStance = mainController.getTableStance();
			if(tableStance != STANCE.NEWORDER_PRODUCTS) {
				int selectedRowIndex = mainTable.getSelectedRow();
				for(int i=0;i<columnCount;i++) {
					rowData[i] = mainTable.getValueAt(selectedRowIndex,i);
				}
				mainController.updateDB(rowData,tableStance);
			}
			else{
				JTable secondaryTable = mainView.getSecondaryTable();
				int rowCount = secondaryTable.getRowCount();
				columnCount = secondaryTable.getColumnCount();
				for(int i=0;i<rowCount;i++) {
					rowData = new Object[columnCount];
					for(int j=0;j<columnCount;j++) {
						rowData[j] = secondaryTable.getValueAt(i, j);
					}
					mainController.updateDB(rowData, tableStance);
				}
			}
			mainView.setInstructiosnText("Database updated!");
		});
	}
	
	/**
	 * creates and returns an action listener for the Print Order button that invokes getInvoiceData method from 
	 * MainController class on a product order id that is retrieved from the selected row of the table 
	 * so as the data can be written into the invoice text file
	 * @return the action listener
	 */
	private ActionListener btnPrintOrderActionListener() {
		return(e->{
			STANCE tableStance = mainController.getTableStance();
			if(tableStance != STANCE.ORDERS) {
				mainView.setInstructiosnText("Select an order, then click Print Order!");
			}
			else {
				JTable mainTable = mainView.getMainTable();
				Integer orderId = Integer.valueOf((String) mainTable.getValueAt(mainTable.getSelectedRow(), 0));
				
				List<String> invoiceData = mainController.getInvoiceData(orderId);
				try {
					PrintWriter outputFile = new PrintWriter("order"+orderId+".txt","UTF-8");
					for(String each : invoiceData) {
						outputFile.println(each);
					}
					outputFile.close();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
			
		});
	}

}
