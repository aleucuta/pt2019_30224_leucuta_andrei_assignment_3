package presentation;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import businessLogicLayer.OrderEntryBLL;
import businessLogicLayer.ProductBLL;
import businessLogicLayer.ProductsOrderBLL;
import model.OrderEntry;
import model.Product;
import model.ProductsOrder;
import presentation.MainController.STANCE;

public class TableController {

	private MainController mainController;
	private MainView mainView;
	private Integer orderId;

	public TableController(MainController mainController, MainView mainView) {
		this.mainController = mainController;
		this.mainView = mainView;
	}

	/**
	 * creates a table having the table data and table column names passed
	 * @param tableData the new table data
	 * @param tableCol the new table column names
	 */
	private void createTable(String[][] tableData, String[] tableCol) {
		final JScrollPane PANEL = mainView.getMainTablePanel();
		// JTable mainTable = mainView.getMainTable();
		JScrollPane mainTablePanel = mainView.getMainTablePanel();
		DefaultTableModel model = new DefaultTableModel(tableData, tableCol);
		final JTable mainTable = new JTable();
		mainTable.setModel(model);

		mainTablePanel.setViewportView(mainTable);
		mainTablePanel.revalidate();
		mainTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON3) {
					Object id = mainTable.getValueAt(mainTable.getSelectedRow(), 0);
					if (id.equals(" "))
						return;
					updateSecondaryTable(Integer.valueOf((String) id));
				}
			}
		});
		mainView.setMainTable(mainTable);

	}

	/**
	 * updates the secondary table based on the program's stance
	 * @param id the id used in updating the table's methods
	 */
	void updateSecondaryTable(Integer id) {
		STANCE tableStance = mainController.getTableStance();
		switch (tableStance) {
		case CLOSED:
			break;
		case CLIENTS:
			secondaryTableClients(id);
			break;
		case ORDERS:
			secondaryTableOrders(id);
			break;
		case NEWORDER_CLIENT:
			mainTableNewOrder(id);
			mainView.setInstructiosnText("Select a product and right click it to add it to the order, fill the empty fields and then click Update to confirm the order!");
			break;
		case NEWORDER_PRODUCTS:
			secondaryTableNewOrder(id);
			break;
		default:
			break;
		}
	}

	/**
	 * creates a new table using createTable, using getColumnNames and getColumnData to populate the table
	 * @param genericClass the class specific to the table
	 * @param genericBLL the class BLL
	 */
	@SuppressWarnings("unchecked")
	void updateMainTable(Class genericClass, Class genericBLL) {
		String[] tableCol = getColumnNames(genericClass);
		String[][] tableData = getColumnData(genericClass, genericBLL);
		createTable(tableData, tableCol);
		
		JTable emptyTable = new JTable();
		mainView.setSecondaryTable(emptyTable);
		JScrollPane secondaryTablePanel = mainView.getSecondaryTablePanel();
		secondaryTablePanel.setViewportView(emptyTable);
		secondaryTablePanel.revalidate();
	}

	/**
	 * (used in the stance neworder_products) updates the main table setting it to products
	 * and the secondary table filling the column names and not filling the table data(it will be added later).
	 * @param id the id of the new order's client
	 */
	private void mainTableNewOrder(Integer id) {
		JScrollPane secondaryTablePanel = mainView.getSecondaryTablePanel();
		updateMainTable(Product.class, ProductBLL.class);
		orderId = newOrderClient(id);
		String[][] tableData = {};
		String[] tableCol = { "Order Id", "Product Id", "Quantity" };
		mainController.setTableStance(STANCE.NEWORDER_PRODUCTS);
		DefaultTableModel model = new DefaultTableModel(tableData, tableCol);
		JTable secondaryTable = new JTable(model);
		secondaryTablePanel.setViewportView(secondaryTable);
		secondaryTablePanel.revalidate();
		mainView.setSecondaryTable(secondaryTable);
	}

	/**
	 * updates the secondary table in the stance neworder_products adding rows with order id and product id, 
	 * and with an empty quantity, it being added there by the program's user
	 * @param id the id of the product to be added into the order
	 */
	private void secondaryTableNewOrder(Integer id) {
		JTable secondaryTable = mainView.getSecondaryTable();
		DefaultTableModel model = (DefaultTableModel) (secondaryTable.getModel());
		boolean ok = true;
		Integer rowCount = secondaryTable.getRowCount();
		for (Integer i = 0; i < rowCount; i++) {
			if (((String)secondaryTable.getValueAt(i, 1)).equals(id.toString()))
				ok = false;
		}
		if (ok == true)
			model.addRow(new Object[] { orderId.toString(), id.toString(), "" });
		mainView.setSecondaryTable(secondaryTable);
	}

	/**
	 * used to obtain details about clients - the orders that they have done
	 * it updates the secondary table with the orders
	 * @param id the client id
	 */
	private void secondaryTableClients(Integer id) {
		JScrollPane secondaryTablePanel = mainView.getSecondaryTablePanel();
		JTable secondaryTable = mainView.getSecondaryTable();
		String[] tableCol = getColumnNames(ProductsOrder.class);
		String[][] tableData = getOrdersForClientId(id);
		secondaryTablePanel.remove(secondaryTable);
		secondaryTable = new JTable(tableData, tableCol);
		secondaryTablePanel.setViewportView(secondaryTable);
		secondaryTablePanel.revalidate();
		mainView.setSecondaryTable(secondaryTable);
	}

	/**
	 * used to obtain details about orders - all the products and their quantities
	 * it updates the secondary table with the products in the order
	 * @param id order's id
	 */
	private void secondaryTableOrders(Integer id) {
		JScrollPane secondaryTablePanel = mainView.getSecondaryTablePanel();
		JTable secondaryTable = mainView.getSecondaryTable();
		String[] tableCol = getColumnNamesForDetailedOrders();
		String[][] tableData = getDataForDetailedOrder(id);
		secondaryTablePanel.remove(secondaryTable);
		secondaryTable = new JTable(tableData, tableCol);
		secondaryTablePanel.setViewportView(secondaryTable);
		secondaryTablePanel.revalidate();
		mainView.setSecondaryTable(secondaryTable);
	}

	/**
	 * invokes updateDB to insert a new order into the database
	 * @param clientId new order's client id
	 * @return the new order's id 
	 */
	private Integer newOrderClient(int clientId) {
		Integer newOrderId = Math.abs(new Random().nextInt() % 1000);
		mainController.updateDB(new Object[] { String.valueOf(newOrderId), String.valueOf(clientId), " ", "0" },
				STANCE.ORDERS);
		return newOrderId;
	}

	/**
	 * gets the column names to create a table from a class' fields 
	 * @param genericClass class to get the fields from
	 * @return a string[] containing fields names to set table column names
	 */
	private <T> String[] getColumnNames(Class<T> genericClass) {

		String[] colums = new String[genericClass.getDeclaredFields().length];
		Integer i = 0;
		for (Field field : genericClass.getDeclaredFields()) {
			field.setAccessible(true);
			colums[i] = field.getName();
			i = i + 1;
		}
		return colums;
	}

	/**
	 * gets orders for a specific client and returns a string[][] containing the orders
	 * @param id client's id
	 * @return a string[][] containing order's data
	 */
	private <T> String[][] getOrdersForClientId(Integer id) {
		ProductsOrderBLL orderBLL = new ProductsOrderBLL();
		List<ProductsOrder> orderList = orderBLL.findProductsOrderForClient(id);
		return fetchData(ProductsOrder.class, orderList);
	}

	/**
	 * gets column names for orders having the same names as Product's fields except stock that is replaced with quantity
	 * @return a string array containing the columnNames
	 */
	private String[] getColumnNamesForDetailedOrders() {
		String[] columnNames = getColumnNames(Product.class);
		Integer length = columnNames.length;
		for (Integer i = 0; i < length; i++) {
			if (columnNames[i].equals("stock") == true) {
				columnNames[i] = "quantity";
			}
		}
		return columnNames;
	}

	/**
	 * gets data for orders and returns a string[][] containing the order's details
	 * gets the data from the product with the id = order.product_id and merges it with order.quantity
	 * @param id order's id
	 * @return returns a string[][] containing the order's details
	 */
	private String[][] getDataForDetailedOrder(Integer id) {
		System.out.println("id = " + id);
		OrderEntryBLL entryBLL = new OrderEntryBLL();
		ProductBLL productBLL = new ProductBLL();
		List<OrderEntry> entries = entryBLL.findOrderEntriesByOrderId(id);
		List<Product> products = new ArrayList<Product>();
		for (OrderEntry each : entries) {
			products.add(productBLL.findProductById(each.getProduct_id()));
		}
		String[][] data = fetchData(Product.class, products);
		Integer length = entries.size();
		for (Integer i = 0; i < length; i++) {
			data[i][4] = Integer.toString(entries.get(i).getQuantity());
		}
		return data;
	}

	/**
	 * a method that receives a class and a list of objects of the class and returns a string[][] containing the
	 * data from the objects
	 * @param genericClass the object's class
	 * @param objectList the objects list
	 * @return a string[][] containing the data from the objects list
	 */
	private <T> String[][] fetchData(Class<T> genericClass, List<T> objectList) {
		String[][] data = new String[objectList.size()][genericClass.getDeclaredFields().length];
		Integer index = 0;
		for (T eachObject : objectList) {
			List<String> line = new ArrayList<String>();
			for (Field field : genericClass.getDeclaredFields()) {
				field.setAccessible(true);
				Object value = null;
				try {
					value = field.get(eachObject);

				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				line.add(value.toString());
			}
			line.toArray(data[index]);
			index++;
		}
		return data;
	}
	
	/**
	 * receives a class and a class bll and invokes the method that lists all the objects
	 * then invokes fetchData on them 
	 * @param genericClass object's class
	 * @param genericBLL class's bll
	 * @return a string[][] containing the data 
	 */
	private <T, W> String[][] getColumnData(Class<T> genericClass, Class<W> genericBLL) {
		try {
			String listMethodName = "list" + genericClass.getSimpleName() + "s";
			Method method = genericBLL.getMethod(listMethodName, null);
			@SuppressWarnings("unchecked")
			W instance = genericBLL.newInstance();
			List<T> objectList = (List<T>) method.invoke(instance, null);
			return fetchData(genericClass, objectList);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}
}
