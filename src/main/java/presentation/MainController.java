package presentation;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import businessLogicLayer.ClientBLL;
import businessLogicLayer.OrderEntryBLL;
import businessLogicLayer.ProductBLL;
import businessLogicLayer.ProductsOrderBLL;
import model.Client;
import model.ProductsOrder;
import model.OrderEntry;
import model.Product;

public class MainController {
	
	private MainView mainView;
	private TableController tableController;
	enum STANCE {CLOSED,CLIENTS,PRODUCTS,ORDERS,NEWORDER_CLIENT,NEWORDER_PRODUCTS,PRINT_ORDER};
	private STANCE tableStance;
	private ActionListenersController actionListenersController;
	public MainController(MainView mainView){
		this.mainView = mainView;
		this.mainView.setVisible(true);
		this.tableController = new TableController(this,mainView);
		this.actionListenersController = new ActionListenersController(this,tableController,mainView);
		actionListenersController.initializeActionListeners();
	}
	/**
	 * Invokes the method to delete an object based on the stance that the program is into.
	 * @param id the id of the object to be deleted.
	 */
	void deleteByStance(Integer id) {
		switch(tableStance) {
			case CLOSED:
				break;
			case CLIENTS:
				ClientBLL clientBLL = new ClientBLL();
				clientBLL.deleteById(id);
			case PRODUCTS:
				ProductBLL productBLL = new ProductBLL();
				productBLL.deleteById(id);
			case ORDERS:
				ProductsOrderBLL productsOrderBLL = new ProductsOrderBLL();
				productsOrderBLL.deleteById(id);
			default:
				break;
		}
	}
	/**
	 * Creates an object with the attributes that are passed 'attributes'
	 * @param attributes the attributes of the newly created object.
	 * @param genericClass the class that you want the new object to be.
	 * @return an object of the class passed, with the attributes passed to the method.
	 */
	private Object createObject(Object[] attributes, Class genericClass) throws InstantiationException, IllegalAccessException, IntrospectionException, IllegalArgumentException, InvocationTargetException {
		Object newObject = genericClass.newInstance();
		Integer i=0;
		for(Field field : genericClass.getDeclaredFields()) {
			PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(),genericClass);
			Method method = propertyDescriptor.getWriteMethod();
			Object attrib = attributes[i++];
			System.out.println(attrib);
			if(field.getGenericType() == Integer.class) {
				attrib = Integer.valueOf((String)attrib);
			}
			if(field.getGenericType() == Double.class) {
				attrib = Double.valueOf((String)attrib);
			}
			method.invoke(newObject, field.getType().cast(attrib));
		}
		return newObject;
	}
	/**
	 * Invokes the method to update an object into the database from the BLL class. 
	 * @param object the updated object
	 * @param id the id to update on
	 * @param genericBLLClass the BLL class that you invoke the method from.
	 */
	private void updateObject(Object object,Integer id,Class genericBLLClass) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException {
		Object genericBLL = genericBLLClass.newInstance();
		for(Method each : genericBLLClass.getMethods()) {
			if(each.getName().startsWith("update") == true && each.getName().endsWith("Id") == true) {
				each.invoke(genericBLL,object,id);
			}
		}
	}
	/**
	 * Invokes the method to insert an object into the database from the BLL class.
	 * @param object the object to be inserted into the database.
	 * @param genericBLLClass the BLL class that you invoke the method from.
	 */
	private void insertObject(Object object,Class genericBLLClass) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IllegalArgumentException {
		Object genericBLL = genericBLLClass.newInstance();
		for(Method each : genericBLLClass.getMethods()) {
			if(each.getName().startsWith("insert") == true) {
				each.invoke(genericBLL, object);
			}
		}
	}
	
	/**
	 * Used to update stuff into the database. It updates into the corresponding table looking at tableStance.
	 * @param rowData The row data to be inserted into the db extracted from one of the tables
	 * @param tableStance The stance that the program is into. The method matches a stance with a defined behavior.
	 */
	void updateDB(Object[] rowData, STANCE tableStance) {
		Class genericClass = Object.class;
		Class genericBLLClass = Object.class;
		switch(tableStance) {
		case CLOSED:
			break;
		case CLIENTS:
			genericClass = Client.class;
			genericBLLClass = ClientBLL.class;
			break;
		case PRODUCTS:
			genericClass = Product.class;
			genericBLLClass = ProductBLL.class;
			break;
		case ORDERS:
			genericClass = ProductsOrder.class;
			genericBLLClass = ProductsOrderBLL.class;
			break;
		case NEWORDER_PRODUCTS:
			genericClass = OrderEntry.class;
			genericBLLClass = OrderEntryBLL.class;
		default:
			break;
		}
		
		try {
			Object genericObject = genericClass.newInstance();
			Object genericBLL = genericBLLClass.newInstance();
			for(Method each : genericBLLClass.getMethods()) {
				if(each.getName().startsWith("find") == true) {
					try {
					genericObject = each.invoke(genericBLL,Integer.parseInt((String)rowData[0]));
					
					//genericObject = ((ClientBLL) genericBLL).findClientById(Integer.valueOf((String)rowData[0]));
					}catch (Exception e) {
						genericObject = null;
					}
					
					if(genericObject == null || tableStance == STANCE.NEWORDER_PRODUCTS) { //creeam un obiect nou
						genericObject = createObject(rowData,genericClass);
						try{
							insertObject(genericObject,genericBLLClass);
						}catch(InvocationTargetException e) {
							mainView.displayErrorMessage("You're doing something wrong!" + "\n" + "Check the console log.");
						}
						return;
					}
					else //updatam obiectul
						genericObject = createObject(rowData,genericClass);
						updateObject(genericObject,Integer.parseInt((String)rowData[0]),genericBLLClass);
						return;
				}
			}
			
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
	}
	
	public STANCE getTableStance() {
		return tableStance;
	}
	
	public void setTableStance(STANCE stance) {
		tableStance = stance;
	}

	public List<String> getInvoiceData(Integer orderId) {
		List<String> invoiceData = new ArrayList<String>();
		ProductsOrderBLL orderBLL = new ProductsOrderBLL();
		ProductsOrder order = orderBLL.findProductsOrderById(orderId);
		ClientBLL clientBLL = new ClientBLL();
		Client client = clientBLL.findClientById(order.getClient_id());
		
		invoiceData.add(new String("\tOrder Invoice"));
		invoiceData.add(new String("--------------------------------------"));
		invoiceData.add(new String("Name : " + client.getFirstName() + " " + client.getLastName()));
		invoiceData.add(new String("Order id :" + order.getId()));
		invoiceData.add(new String("Address : " + order.getAddress()));
		invoiceData.add(new String("--------------------------------------"));
		
		OrderEntryBLL entryBLL = new OrderEntryBLL();
		List<OrderEntry> entries = entryBLL.findOrderEntriesByOrderId(order.getId());
		ProductBLL productBLL = new ProductBLL();
		int productIndex = 1;
		double total = 0;
		for(OrderEntry entry : entries) {
			Product product = productBLL.findProductById(entry.getProduct_id());
			invoiceData.add(new String(productIndex + "." + product.getName() + " x" + entry.getQuantity()));
			invoiceData.add(new String("\t" + entry.getQuantity() + "x " + product.getPrice() + "\t\t" + product.getPrice() * entry.getQuantity()));
			total = total + product.getPrice() * entry.getQuantity();
		}
		invoiceData.add(new String("--------------------------------------"));
		invoiceData.add(new String("TOTAL = " + total));
		invoiceData.add(new String("--------------------------------------"));
		invoiceData.add(new String("Thanks for shopping with us!\nHope you have a great day!"));
		return invoiceData;
	}
	
}
